package com.example.adminlogin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.Toast;

import com.example.adminlogin.EmployeeActivites.EmployeeHomeActivity;

//http://angrytools.com/android/button/
public class AdminToMe extends AppCompatActivity {

    GridLayout gridLayout;
    Intent it;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admintome);

        gridLayout=(GridLayout)findViewById(R.id.mainGrid);
        setSingleEvent(gridLayout);
    }


    private void setSingleEvent(GridLayout gridLayout) {
        for (int i = 0; i < gridLayout.getChildCount(); i++) {
            CardView cardView = (CardView) gridLayout.getChildAt(i);
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Toast.makeText(AdminToMe.this, "Clicked at index " + finalI, Toast.LENGTH_SHORT).show();

                    if(finalI == 0){
                        it = new Intent(getApplicationContext(),Reports.class);
                        startActivity(it);
                    }
                    if(finalI == 1){
                        it = new Intent(getApplicationContext(),Department.class);
                        startActivity(it);
                    }
                    if(finalI == 2){
                        it = new Intent(getApplicationContext(),Offices.class);
                        startActivity(it);
                    }
                    if(finalI == 3){
                        it = new Intent(getApplicationContext(),Designations.class);
                        startActivity(it);
                    }
                    if(finalI == 4){
                        it = new Intent(getApplicationContext(),EmployeeActivity.class);
                        startActivity(it);
                    }
                }
            });
        }
    }
}
