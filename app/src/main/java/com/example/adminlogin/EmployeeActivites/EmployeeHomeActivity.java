package com.example.adminlogin.EmployeeActivites;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.Toast;

import com.example.adminlogin.Calender;
import com.example.adminlogin.CollectorHomePage;
import com.example.adminlogin.DepartmentAddFragment;
import com.example.adminlogin.DepartmentViewFragment;
import com.example.adminlogin.HistoryActivity;
import com.example.adminlogin.R;

public class EmployeeHomeActivity extends AppCompatActivity {

    GridLayout gridLayout;
    Intent it;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee_home);

        gridLayout=(GridLayout)findViewById(R.id.mainGrid);
        setSingleEvent(gridLayout);

    }

    private void setSingleEvent(GridLayout gridLayout) {
        for (int i = 0; i < gridLayout.getChildCount(); i++) {
            CardView cardView = (CardView) gridLayout.getChildAt(i);
            final int finalI = i;
            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(EmployeeHomeActivity.this, "Clicked at index " + finalI,
                            Toast.LENGTH_SHORT).show();
                    if (finalI == 0) {
                        it = new Intent(getApplicationContext(), OverviewActivity.class);
                        startActivity(it);
                    }
                    if (finalI == 1) {
                        it = new Intent(getApplicationContext(), ApplyLeaveActivity.class);
                        startActivity(it);
                    }
                    if (finalI == 2) {
                        it = new Intent(getApplicationContext(), Calender.class);
                        startActivity(it);
                    }
                    if (finalI == 3) {
                        it = new Intent(getApplicationContext(),HistoryActivity.class);
                        startActivity(it);
                    }

                }
            });
        }
    }
}
