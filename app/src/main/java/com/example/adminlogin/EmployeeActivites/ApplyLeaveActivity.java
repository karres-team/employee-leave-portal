package com.example.adminlogin.EmployeeActivites;

import androidx.appcompat.app.AppCompatActivity;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.AdapterView;

import com.example.adminlogin.R;

import java.util.Calendar;

public class ApplyLeaveActivity extends AppCompatActivity {

    DatePickerDialog picker;
    EditText eText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_leave);

        Spinner lt = (Spinner)findViewById(R.id.lvTyp);
        Spinner sp = (Spinner)findViewById(R.id.slctPrps);
        eText=(EditText) findViewById(R.id.editText1);

        eText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar cldr = Calendar.getInstance();
                int day = cldr.get(Calendar.DAY_OF_MONTH);
                int month = cldr.get(Calendar.MONTH);
                int year = cldr.get(Calendar.YEAR);
                // date picker dialog
                picker = new DatePickerDialog(ApplyLeaveActivity.this,
                        new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                eText.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);
                            }
                        }, year, month, day);
                picker.show();
            }
        });

        String [] LeaveType = {"Casual Leave","Personal Leave","Sick Leave","Others"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, LeaveType);
        adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        lt.setAdapter(adapter);

        String [] selectPurpose = {"Not feeling well","Need to take care of personal errands","Goging out of station on personal work","Others"};

        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item,selectPurpose);
        adapter1.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        sp.setAdapter(adapter1);

    }
}
