package com.example.adminlogin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Department extends AppCompatActivity {

    Button addDprtFrgmnt,viewDprtFrgmnt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_department);

        viewDprtFrgmnt = (Button) findViewById(R.id.viewDprtmnt);
        addDprtFrgmnt = (Button) findViewById(R.id.addDprtmnt);

        final Fragment viewDepartment = new DepartmentViewFragment();
        final Fragment addDepartment = new DepartmentAddFragment();

        viewDprtFrgmnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.linearLayout,viewDepartment);
                ft.commit();
            }
        });
        addDprtFrgmnt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.linearLayout,addDepartment);
                ft.commit();
            }
        });
    }
}
