package com.example.adminlogin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.Toast;

import static android.graphics.Color.*;

public class CollectorHomePage extends AppCompatActivity {

    GridLayout gridLayout;
    Intent it;
    CardView cardView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_collector_home_page);

        gridLayout = (GridLayout) findViewById(R.id.mainGrid);
        setSingleEvent(gridLayout);
        //setToggleEvent(gridLayout);

    }

    private void setSingleEvent (GridLayout gridLayout) {
        for (int i = 0; i < gridLayout.getChildCount(); i++) {
            cardView = (CardView) gridLayout.getChildAt(i);
            final int finalI = i;

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(CollectorHomePage.this, "Clicked at index " + finalI,
                            Toast.LENGTH_SHORT).show();
                    if (finalI == 0) {
                        it = new Intent(getApplicationContext(), Requests.class);
                        startActivity(it);
                    }
                    if (finalI == 1) {
                        it = new Intent(getApplicationContext(), Approvals.class);
                        startActivity(it);
                    }
                    if (finalI == 2) {
                        it = new Intent(getApplicationContext(), Rejections.class);
                        startActivity(it);
                    }
                    if (finalI == 3) {
                        it = new Intent(getApplicationContext(), Calender.class);
                        startActivity(it);
                    }
                }
            });
        }
    }
}

