package com.example.adminlogin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class EmployeeActivity extends AppCompatActivity {

    Button viewEmply,addEmply;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_employee);

        viewEmply = (Button) findViewById(R.id.viewEmp);
        addEmply = (Button) findViewById(R.id.addEmp);

        final Fragment viewEmployee = new EmployeeListFragment();
        final Fragment addEmployee = new EmployeeAddFragment();

        viewEmply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.linearLayout,viewEmployee);
                ft.commit();
            }
        });

        addEmply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.linearLayout,addEmployee);
                ft.commit();
            }
        });
    }
}
