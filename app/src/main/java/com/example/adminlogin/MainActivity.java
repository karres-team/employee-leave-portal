package com.example.adminlogin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.adminlogin.EmployeeActivites.EmployeeHomeActivity;

public class MainActivity extends AppCompatActivity {

    EditText userName,password;
    Button login;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userName = (EditText)findViewById(R.id.username);
        password = (EditText)findViewById(R.id.password);
        login = (Button)findViewById(R.id.login);


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String usrName = userName.getText().toString();
                String passWrd = password.getText().toString();

                Log.d("username ", usrName);
                Log.d("Password ", passWrd);

                Toast.makeText(getApplication(),"Button Clicked ",Toast.LENGTH_SHORT).show();

                if(usrName.equalsIgnoreCase("admin") && passWrd.equalsIgnoreCase("pass@123")) {
                    Intent it = new Intent(getApplicationContext(), AdminToMe.class);
                    startActivity(it);
                }
                if(usrName.equalsIgnoreCase("coll25")  && passWrd.equalsIgnoreCase("pass@123")){
                    Intent it = new Intent(getApplicationContext(), CollectorHomePage.class);
                    startActivity(it);
                }
                if(usrName.equalsIgnoreCase("emp25") && passWrd.equalsIgnoreCase("pass@123")){
                    Intent it = new Intent(getApplicationContext(), EmployeeHomeActivity.class);
                    startActivity(it);
                }
            }
        });
    }
}
