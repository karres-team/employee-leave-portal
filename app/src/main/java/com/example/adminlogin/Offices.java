package com.example.adminlogin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Offices extends AppCompatActivity {

    Button addOffices,viewOffices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_offices);

        viewOffices = (Button) findViewById(R.id.viewOffices);
        addOffices = (Button) findViewById(R.id.addOffices);

        final Fragment viewOffice = new OfficeListFragment();
        final Fragment addOffice = new OfficeAddFragment();

        viewOffices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.linearLayout,viewOffice);
                ft.commit();
            }
        });
        addOffices.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.linearLayout,addOffice);
                ft.commit();
            }
        });
    }
}
