package com.example.adminlogin;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Designations extends AppCompatActivity {

    Button addDsgntns,viewDsgntns;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_designations);

        viewDsgntns = (Button) findViewById(R.id.viewDsgntn);
        addDsgntns = (Button) findViewById(R.id.addDsgntn);

        final Fragment viewDsgntn = new DesignationListFragment();
        final Fragment addDsgntn = new DesignationAddFragment();

        viewDsgntns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.linearLayout,viewDsgntn);
                ft.commit();
            }
        });
        addDsgntns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager fm = getSupportFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                ft.replace(R.id.linearLayout,addDsgntn);
                ft.commit();
            }
        });

    }
}
